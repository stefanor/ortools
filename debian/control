Source: ortools
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Vcs-Git: https://salsa.debian.org/science-team/ortools.git
Vcs-Browser: https://salsa.debian.org/science-team/ortools
Uploaders:
 Agathe Porte <debian@microjoe.org>,
Build-Depends:
 cmake,
 python3-dev,
 coinor-libcbc-dev,
 coinor-libcgl-dev,
 coinor-libclp-dev,
 coinor-libcoinutils-dev,
 coinor-libosi-dev,
 debhelper-compat (= 13),
 doxygen <!nodoc>,
 graphviz <!nodoc>,
 libabsl-dev,
 libprotobuf-dev,
 mypy-protobuf,
 pkg-config,
 protobuf-compiler,
 python3-absl,
 python3-protobuf,
 python3-setuptools,
 python3-venv,
 python3-wheel,
 swig,
 zlib1g-dev,
Standards-Version: 4.6.0
Homepage: https://github.com/google/or-tools
Rules-Requires-Root: no

Package: libortools-dev
Architecture: any
Section: libdevel
Depends:
 coinor-libcoinutils-dev,
 libabsl-dev,
 libprotobuf-dev,
 zlib1g-dev,
 ${misc:Depends},
Description: Google Optimization Tools (development files)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains the development files for the ortools library.

Package: libortools8
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Google Optimization Tools (library)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains the ortools library.

Package: libortools-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
Description: Google Optimization Tools (library documentation)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains Doxygen documentation for OR-Tools.

Package: ortools-examples
Architecture: all
Depends:
 ${misc:Depends},
Description: Google Optimization Tools (examples)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains example usage of ortools for multiple usecases as well
 as multiple language bindings.

Package: ortools-flatzinc
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Google Optimization Tools (flatzinc)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains the flatzinc binary and minizinc files.

Package: ortools-samples
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Google Optimization Tools (samples)
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains sample programs using ortools.

Package: python3-ortools
Architecture: any
Section: python
Depends:
 libortools8 (= ${binary:Version}),
 python3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Google Optimization Tools (Python library)
 This package contains the Python bindings for OR-Tools.
 .
 Google Optimization Tools (a.k.a., OR-Tools) is an open-source, fast and
 portable software suite for solving combinatorial optimization problems.
 .
 The suite contains:
 .
  * A constraint programming solver;
  * A linear programming solver;
  * Wrappers around commercial and other open source solvers, including mixed
    integer solvers;
  * Bin packing and knapsack algorithms;
  * Algorithms for the Traveling Salesman Problem and Vehicle Routing Problem;
  * Graph algorithms (shortest paths, min cost flow, max flow, linear sum
    assignment).
 .
 OR-Tools is written in C++, but also provide wrappers in Python, C# and Java.
 .
 This package contains the Python 3 bindings of ortools.
